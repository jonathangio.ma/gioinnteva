<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'username', 'email', 'name', 'paternal_surname', 'maternal_surname', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roleuser()
    {
        # code...
        return $this->belongsTo(Role::class, 'role_id');
    }


    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_users');
    }

    public function hasAccess(array $permissions){
        foreach ($this->roles as $role) {
            # code...
            if($role->hasAccess($permissions)){
                return true;
            }
        }
        return false;
    }


    //Api json
    //Set form json partners
    public function allUsers()
    {
        # code...
        return self::all();
    }
}
