<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerUserPolicies();

        //
    }

    public function registerUserPolicies(){
        //Crear
        Gate::define('create-user', function($user){
            return $user->hasAccess(['create-user']);
        });

        Gate::define('update-user', function ($user) {
            return $user->hasAccess(['update-user']);
        });

        Gate::define('active-user', function ($user) {
            return $user->hasAccess(['active-user']);
        });

        Gate::define('delete-user', function ($user) {
            return $user->hasAccess(['delete-user']);
        });



                //'index-user' => true,
                //'create-user' => true,
                //'active-user' => true,
                //'delete-user' => true,
                //'update-user' => true
    }
}
