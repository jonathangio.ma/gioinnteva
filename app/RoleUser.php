<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    //
    protected $table = "role_users";

    protected $fillable = [
        'user_id', 'role_id'
    ];

    public function user()
    {
        # code...
        return $this->belongsTo(User::class, 'user_id');
    }

    public function role()
    {
        # code...
        return $this->belongsTo(Role::class, 'role_id');
    }
}
