<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    protected $table = "roles";

    protected $fillable = [
        'name', 'slug', 'permissions',
    ];


    public function users(){
        return $this->belongsToMany(User::class, 'role_users');
    }

    public function hasAccess(array $permissions)
    {
        foreach ($permissions as $permission) {
            # code...
            if ($this->hasPermisions($permission)) {
                return true;
            }
        }
        return false;
    }


    public function hasPermisions(string $permission)
    {
        $permissions = json_decode($this->permissions, true);

        return $permissions[$permission]??false;
    }

}
