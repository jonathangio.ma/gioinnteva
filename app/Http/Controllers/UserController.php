<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\RoleUser;
class UserController extends Controller
{
    protected $user = null;

    public function __construct(User $user)
    {
        # code...
        $this->user = $user;
    }


    public function api()
    {
        //
        return \Response::json($this->user->allUsers(), 200);
    }

    public function activeuser(){
        //
        $data = User::where('active', 1)->get();
        return view('users.activeuser', ['data' => $data]);
    }

    public function inactiveuser()
    {
        //
        $data = User::where('active', 0)->get();
        return view('users.listinactive', ['data' => $data]);
    }

    public function admin()
    {
        //
        $data = RoleUser::where('role_id', 1)->get();

        $data->each(function ($data) {
            # code...
            $data->user;
            $data->role;
        });
        return view('users.admin', ['data' => $data]);
    }
    public function opera()
    {
        //
        $data = RoleUser::where('role_id', 2)->get();

        $data->each(function ($data) {
            # code...
            $data->user;
            $data->role;
        });
        return view('users.opera', ['data' => $data]);
    }
    public function develop()
    {
        //
        $data = RoleUser::where('role_id', 3)->get();

        $data->each(function ($data) {
            # code...
            $data->user;
            $data->role;
        });
        return view('users.develop', ['data' => $data]);
    }


    public function rolesdata()
    {
        //
        $data = RoleUser::all();

        $data->each(function ($data) {
            # code...
            $data->user;
            $data->role;
        });

        //dd($data);
        return view('users.alldatauser', ['data' => $data]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = RoleUser::all();

        $data->each(function ($data) {
            # code...
            $data->user;
            $data->role;
        });

        //dd($data);
        return view('users.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $role = Role::orderBy('name', 'desc')->pluck('name', 'id');
        //Relaciones

        return view('users.create', ['roles' => $role]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = \Validator::make($request->all(), [
            'username' => 'required|unique:users|max:255',
            'email' => 'required|unique:users|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Verify Unique Params Email, Username ');
        }

        $user = User::create([
            'username' => $request->username,
            'email' => $request->email,
            'name' => $request->names,
            'paternal_surname' => $request->paternal_surname,
            'maternal_surname' => $request->maternal_surname,
            'age' => $request->age,
            'password' => \Hash::make('password'),
            'active' => $request->active,
        ]);

        $user->roles()->attach($request->role);

        if ($user) {
            # code...
            return redirect()->back()->with('status', 'You uSER ADD  successfully');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function search(){
         return view('users.search');
     }
    public function show(Request $request)
    {
        //
        $data = User::find($request->user_id);


        if($data){
            return view('users.show', ['user' => $data])->with('status', 'Success');
        }else{
            return redirect()->back()->with('status', 'Not result ' . $request->user_id);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::find($id);
        $roleuser = RoleUser::where('user_id', $id)->first();

        $role = Role::orderBy('name', 'desc')->pluck('name', 'id');

        return view('users.edit', ['user' => $user, 'roles' => $role, 'roleasign' => $roleuser]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = User::find($id);
        $user->username          = $request->username;
        $user->name          = $request->names;
        $user->email       = $request->email;
        $user->active   = $request->active;
        $user->paternal_surname           = $request->paternal_surname;
        $user->maternal_surname       = $request->maternal_surname;
        $user->age         = $request->age;

        $roleuser = RoleUser::where('user_id', $id)->first();
        $roleuser->role_id = $request->role;
        $roleuser->save();


        if ($user->save()) {
            return redirect()->back()->with('status', 'Updated ' . $user->username . ' Success.');
        }
    }

    public function active(Request $request)
    {
        //

        $user = User::find($request->user_id);
        $user->active   = $request->active;

        if ($user->save()) {
            return redirect()->back()->with('status', 'Active Updated ' . $user->username . ' Success.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::find($id);
        if ($user->delete()) {
            # code...
            return redirect()->back()->with('status', 'User delete success');
        }
    }
}
