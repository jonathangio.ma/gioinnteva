<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $gio = Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
            'permissions' => json_encode([
                'index-user' => true,
                'create-user' => true,
                'active-user' => true,
                'delete-user' => true,
                'update-user' => true
            ])
        ]);

        $gio = Role::create([
            'name' => 'Operador',
            'slug' => 'operador',
            'permissions' => json_encode([
                'index-user' => true,
                'create-user' => false,
                'active-user' => false,
                'delete-user' => false,
                'update-user' => false
            ])
        ]);

        $gio = Role::create([
            'name' => 'Desarrollador',
            'slug' => 'develop',
            'permissions' => json_encode([
                'index-user' => true,
                'create-user' => true,
                'active-user' => false,
                'delete-user' => false,
                'update-user' => true
            ])
        ]);
    }
}
