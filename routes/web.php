<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/users', 'UserController');
Route::post('/users/active', 'UserController@active')->name('users.active');

Route::get('/data/active', 'UserController@activeuser')->name('users.list.active');
Route::get('/data/inactive', 'UserController@inactiveuser')->name('users.list.inactive');

Route::get('/data/admin', 'UserController@admin')->name('users.list.admin');
Route::get('/data/opera', 'UserController@opera')->name('users.list.opera');
Route::get('/data/develop', 'UserController@develop')->name('users.list.develop');

Route::get('/data/detail', 'UserController@rolesdata')->name('users.list.rolesdata');
Route::post('/data/show/', 'UserController@show')->name('users.list.show');
Route::get('/data/seacrh', 'UserController@search')->name('users.list.search');


