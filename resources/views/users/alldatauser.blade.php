@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">


                <div class="card-header">Lista de Usuarios
                    @can('create-user')
                        <a href="{{ route('users.create') }}">
                        <button type="button" class="btn btn-warning">Crear usuario</button></a>
                    @endcan
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif



                    <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">paternal_surname</th>
                        <th scope="col">maternal_surname</th>
                        <th scope="col">Username</th>
                        <th scope="col">Role</th>
                        <th scope="col">Permissions</th>


                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $val)
                        <tr>
                            <th scope="row">{{ $val->id }}</th>
                            <td>{{ $val->user->name }}</td>
                            <td>{{ $val->user->paternal_surname }}</td>
                            <td>{{ $val->user->maternal_surname }}</td>
                            <td>{{ $val->user->username }}</td>
                            <td>{{ $val->role->name }}</td>
                            <td>{{ $val->role->permissions }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
