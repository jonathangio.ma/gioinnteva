@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">


                <div class="card-header">Lista de Usuarios
                    @can('create-user')
                        <a href="{{ route('users.create') }}">
                        <button type="button" class="btn btn-warning">Crear usuario</button></a>
                    @endcan
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                                {{ Form::open(['route' => ['users.list.show'], 'method' => 'POST',  'style' => '    display: initial;']) }}
                                <input type="text" class="form-control mb-5" name="user_id" placeholder="Ingresa el Id del Usuario">

                                <button class="btn btn-icon btn-3 btn-danger" type="submit">Buscar</button>
                                {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
