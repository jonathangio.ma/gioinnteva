@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
 @if (session('status'))
               <div class="alert alert-success" role="alert">
                  {{ session('status') }}
               </div>
               @endif

                <div class="card-header">Lista de Usuarios
                    @can('create-user')
                        <a href="{{ route('users.create') }}">
                        <button type="button" class="btn btn-warning">Crear usuario</button></a>
                    @endcan
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif



                        <ul class="list-group">
                        <li class="list-group-item"><b>Name</b> {{ $user->name }}</li>
                        <li class="list-group-item"><b>Username</b>  {{ $user->name }}</li>
                        <li class="list-group-item"><b>Paternal_surname</b> {{ $user->name }}</li>
                        <li class="list-group-item"><b>Maternal_surname</b> {{ $user->name }}</li>


                        </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
