@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-8">
         <div class="card">
            <div class="card-header">Crear de Usuarios</div>
            <div class="card-body">
               @if (session('status'))
               <div class="alert alert-success" role="alert">
                  {{ session('status') }}
               </div>
               @endif

               @if (session('error'))
               <div class="alert alert-danger" role="alert">
                  {{ session('error') }}
               </div>
               @endif

               {{-- Crear --}}
               {!! Form::open(['route' => 'users.store', 'method' => 'POST']) !!}
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group label-floating">
                        <label class="control-label">Username<small>*</small></label>
                        {{ Form::text('username', null,['class' => 'form-control', 'id' => 'username', 'required' => 'true', 'arial-required' => 'true']) }}
                        <span class="material-input"></span>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group label-floating">
                        <label class="control-label">Names<small>*</small></label>
                        {{ Form::text('names', null,['class' => 'form-control', 'id' => 'names', 'required' => 'true', 'arial-required' => 'true']) }}
                        <span class="material-input"></span>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group label-floating">
                        <label class="control-label">paternal_surname<small>*</small></label>
                        {{ Form::text('paternal_surname', null,['class' => 'form-control', 'id' => 'paternal_surname', 'required' => 'true', 'arial-required' => 'true']) }}
                        <span class="material-input"></span>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group label-floating">
                        <label class="control-label">maternal_surname<small>*</small></label>
                        {{ Form::text('maternal_surname', null,['class' => 'form-control', 'id' => 'maternal_surname', 'required' => 'true', 'arial-required' => 'true']) }}
                        <span class="material-input"></span>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group label-floating">
                        <label class="control-label">Email<small>*</small></label>
                        {{ Form::text('email', null,['class' => 'form-control', 'id' => 'email', 'required' => 'true', 'arial-required' => 'true']) }}
                        <span class="material-input"></span>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group label-floating">
                        <label class="control-label">Age<small>*</small></label>
                        {{ Form::text('age', null,['class' => 'form-control', 'id' => 'age', 'required' => 'true', 'arial-required' => 'true']) }}
                        <span class="material-input"></span>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group label-floating">
                        <label class="control-label">Rol<small>*</small></label>
                        {{ Form::select('role', $roles, null, [ 'class' => 'form-control', 'title' => 'Elige un Rol',  'id' => 'role', 'required' => 'true', 'arial-required' => 'true' ]) }}
                        <span class="material-input"></span>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group label-floating">
                        <label class="control-label">Active<small>*</small></label>
                        <select class="form-control" name="active" data-style="select-with-transition" title="Choose ACTIVE">
                            <option disabled> Choose ACTIVE</option>
                            <option value="1" selected="true">YES </option>
                            <option value="0">NO</option>
                        </select>
                        <span class="material-input"></span>
                     </div>
                  </div>
               </div>
               <button type="submit" class="btn btn-success btn-round" >Save</button>
               {!! Form::close() !!}
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
