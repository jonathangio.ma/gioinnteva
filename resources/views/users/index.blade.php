@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">


                <div class="card-header">Lista de Usuarios
                    @can('create-user')
                        <a href="{{ route('users.create') }}">
                        <button type="button" class="btn btn-warning">Crear usuario</button></a>
                    @endcan
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif



                    <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Username</th>
                        <th scope="col">Role</th>
            
                        <th scope="col">Editar</th>
                        <th scope="col">Activar</th>
                        <th scope="col">Delete</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $val)
                        <tr>
                            <th scope="row">{{ $val->id }}</th>
                            <td>{{ $val->user->name }}</td>
                            <td>{{ $val->user->username }}</td>
                            <td>{{ $val->role->name }}</td>

                            <td>
                                @can('update-user')
                                <a href="{{ url('users/'.$val->user->id.'/edit') }}" class="btn btn-primary">Edit</a>
                                @endcan
                            </td>
                            <td>
                                @if($val->user->active == 0)
                                    @can('active-user')
                                    {{ Form::open(['route' => ['users.active'], 'method' => 'POST', 'style' => '    display: initial;']) }}
                                        <input type="hidden" name="user_id" value="{{ $val->user->id }}">
                                        <input type="hidden" name="active" value="1">
                                        <button class="btn btn-icon btn-3 btn-success" type="submit">Active</button>
                                    {{ Form::close() }}
                                    @endcan
                                @else
                                    @can('active-user')
                                    {{ Form::open(['route' => ['users.active'], 'method' => 'POST', 'style' => '    display: initial;']) }}
                                        <input type="hidden" name="user_id" value="{{ $val->user->id }}">
                                        <input type="hidden" name="active" value="0">
                                        <button class="btn btn-icon btn-3 btn-default" type="submit">Inactive</button>
                                    {{ Form::close() }}
                                    @endcan
                                @endif
                            </td>
                            <td>
                                @can('delete-user')

                                {{ Form::open(['route' => ['users.destroy', $val->user->id], 'method' => 'DELETE', 'onsubmit' => 'return ConfirmDelete()', 'style' => '    display: initial;']) }}
                                 <button class="btn btn-icon btn-3 btn-danger" type="submit">Delete</button>
                                {{ Form::close() }}
                                @endcan
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
