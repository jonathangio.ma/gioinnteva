@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-8">
         <div class="card">
            <div class="card-header">Editar de Usuarios</div>
            <div class="card-body">
               @if (session('status'))
               <div class="alert alert-success" role="alert">
                  {{ session('status') }}
               </div>
               @endif

               @if (session('error'))
               <div class="alert alert-danger" role="alert">
                  {{ session('error') }}
               </div>
               @endif

               {{-- Crear --}}
               {!! Form::open(['route' => ['users.update', $user->id], 'method' => 'PUT']) !!}
               <div class="row">
                  <div class="col-md-6">
                     <div class="form-group label-floating">
                        <label class="control-label">Username<small>*</small></label>
                        {{ Form::text('username', $user->username ,['class' => 'form-control', 'id' => 'username', 'required' => 'true', 'arial-required' => 'true']) }}
                        <span class="material-input"></span>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group label-floating">
                        <label class="control-label">Names<small>*</small></label>
                        {{ Form::text('names', $user->name,['class' => 'form-control', 'id' => 'names', 'required' => 'true', 'arial-required' => 'true']) }}
                        <span class="material-input"></span>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group label-floating">
                        <label class="control-label">paternal_surname<small>*</small></label>
                        {{ Form::text('paternal_surname', $user->paternal_surname,['class' => 'form-control', 'id' => 'paternal_surname', 'required' => 'true', 'arial-required' => 'true']) }}
                        <span class="material-input"></span>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group label-floating">
                        <label class="control-label">maternal_surname<small>*</small></label>
                        {{ Form::text('maternal_surname', $user->maternal_surname,['class' => 'form-control', 'id' => 'maternal_surname', 'required' => 'true', 'arial-required' => 'true']) }}
                        <span class="material-input"></span>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group label-floating">
                        <label class="control-label">Email<small>*</small></label>
                        {{ Form::text('email', $user->email,['class' => 'form-control', 'id' => 'email', 'required' => 'true', 'arial-required' => 'true']) }}
                        <span class="material-input"></span>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group label-floating">
                        <label class="control-label">Age<small>*</small></label>
                        {{ Form::text('age', $user->age,['class' => 'form-control', 'id' => 'age', 'required' => 'true', 'arial-required' => 'true']) }}
                        <span class="material-input"></span>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group label-floating">
                        <label class="control-label">Rol<small>*</small></label>
                        {{ Form::select('role', $roles, $roleasign->role_id, [ 'class' => 'form-control', 'title' => 'Elige un Rol',  'id' => 'role', 'required' => 'true', 'arial-required' => 'true' ]) }}
                        <span class="material-input"></span>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group label-floating">
                        <label class="control-label">Active<small>*</small></label>
                        @if($user->active == 1)
                        <select class="form-control" name="active" data-style="select-with-transition" title="Choose City" data-size="7">
                            <option disabled> Choose city</option>
                            <option value="1" selected="true">YES </option>
                            <option value="0">NO</option>
                        </select>
                        @else
                        <select class="form-control" name="active" data-style="select-with-transition" title="Choose City" data-size="7">
                            <option disabled> Choose city</option>
                            <option value="1">YES </option>
                            <option value="0" selected="true">NO</option>
                        </select>
                        @endif
                        <span class="material-input"></span>
                     </div>
                  </div>
               </div>
               <button type="submit" class="btn btn-success btn-round" >Save</button>
               {!! Form::close() !!}
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
